# Table of Resources

This document contains each of the resources used from external sources, along with the links to the originals.

| Resource | Link |
| -------- | ---- |
| icons/icon-60.png | https://www.iconfinder.com/icons/130203/view_checkup_eyeglasses_eye_look_sightless_unseeing_glass_see_spectacles_viewless_glasses_eyeless_icon |
| icons/icon-48.png | Resized from icons/icon-60.png |
| icons/icon-96.png | Resized from icons/icon-60.png |
