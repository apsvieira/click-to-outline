# Click to Outline

Browser extension that opens a page via [Outline](outline.com) with one click.

Code is licensed with [MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/).

## Disclaimer

I have no association with the developers of Outline, and this extension was developed exclusively by me. The only aim of this extension is to save a few moments of copy-pasting.

## Functionality

This extension provides two ways of opening webpages using Outline:

1. Click on the navigation bar button to open the current page;
2. Right-click any link to open it via Outline;

## Support

- Firefox via Add-On: Install [here](https://addons.mozilla.org/en-US/firefox/addon/click-to-outline/).
- Firefox Mobile
- Chrome: Install from unpacked source, see below.

### Installing on Chrome

To install from the unpacked source on Chrome:

1. Download the source code (via Git or otherwise)
2. Open a tab on `chrome://extensions`
3. Click "Load unpacked" and select the "click-to-outline" directory.
