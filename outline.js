function onTabCreated(tab) {
    console.log(`Created new tab: ${tab.id}`);
}

function openOnOutline(url) {
    let creating = browser.tabs.create({
        url: "https://outline.com/" + url,
        active: true,
    });
    creating.then(onTabCreated).catch(
        error => console.err(`Failed to create tab. Error: ${error}`)
    );
}

function outlineCurrentTab() {
    let gettingActiveTab = browser.tabs.query({
        active: true, currentWindow: true
    });
    gettingActiveTab.then((tabs) => {
        if (tabs[0]) {
            let activeTab = tabs[0];
            openOnOutline(activeTab.url);
        } else {
            console.log(`No active tab.`);
        }
    }).catch(
        error => console.err(`Failed to get active tab ${error}`)
    );
}

// Action button on Navigation Tab
browser.browserAction.onClicked.addListener(outlineCurrentTab);

// Button on context menu: click to open link.
browser.contextMenus.create({
    id: "click-to-outline",
    title: "Click to Outline!",
    contexts: ["link"],
});

browser.contextMenus.onClicked.addListener(function (info, tab) {
    if (info.menuItemId == "click-to-outline") {
        openOnOutline(info.linkUrl);
    }
})
