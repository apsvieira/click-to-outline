# CHANGELOG

## [1.1.0] - 2021-12-09
### Added
- Context-menu option to open links on new tabs using Outline

## [1.0.1] - 2021-12-08
### Added
- License file specifying usage of MPL 2.0
- Chrome support using WebExtension browser API Polyfill

## [1.0.0] - 2021-12-07
### Added
- Firefox extension with a button that opens the current page using Outline
